DESCRIPTION
===========

Internet Explorer has a limit for the maximum number of css selectors in a
file (4095). If one of the css groups has more than 4095 selectors this module
will split the css in multiple files during the css file aggregation.
